<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package AEO
 */

get_header(); ?>

			<div class="vidMain">
				<div class="vidContainer ios">
				</div>
				<div class="vidContent">
					<h1><img src="<?php echo get_template_directory_uri(); ?>/imgs/mwy.png"></h1>
					<p class="lead"><small>#MOVESWITHYOU</small></p>
<!-- 					<p><a onmousedown="ga('send', 'event', 'Video', 'click', 'MovesWithYouReproducir');" class="btn btn-lg btn-default" id="video-toggle" href="#" role="button">PLAY VIDEO</a></p> -->
				</div>
			</div>

<?php // ?>

			<div class="hide" style="
			    padding: 30px 20%;
			    font-size: 1.5em;
			    color: #001D3B;
			    text-align: center;
			    line-height: normal;
			    text-transform: uppercase;
			"><p style="
			    margin: 0;
			">¡Tener tu AEO Card te trae grandes beneficios! Pide la tuya en la compra de cualquier Denim X o Flex/Denim en Acoxpa, Buenavista, Perisur, Toreo y Santa Fe.</p></div>

			<div class="container" style="display:none">
				<div class="row">
					<div class="col-sm-12">
						<div class="parContent">
						<!-- <h3 class="subt">DINÁMICA</h3> -->
						<p>A PARTIR DE ESTE VERANO AEO SE MUEVE CONTIGO. <br> MUY PRONTO TENDRÁS BENEFICIOS EN LOS MEJORES LUGARES.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="hidden-xs hide">
					<div class="container promos">



								<?php $loop = new WP_Query( array( 'post_type' => 'promociones', 'posts_per_page' => 20, 'order' => 'ASC' ) ); ?>
								<?php $count=0; ?>
								<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
									<?php $count++; ?>
								<?php endwhile; wp_reset_query(); ?>

								<?php $quant = 10-$count; ?>
								<?php 
								$x = 1; 

								while($x <= $quant) {
								    //echo "<div class='promo-block bg-block flip-container'>$x</div>";
								    $x++;
								} 
								?>
								<?php $loop = new WP_Query( array( 'post_type' => 'promociones', 'posts_per_page' => 20, 'order' => 'ASC' ) ); ?>
								<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

								<?php
									$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

									$post_thumbnail_id = get_post_thumbnail_id( $post_id );
									$imgmeta = wp_get_attachment_metadata( $post_thumbnail_id );
									if ($imgmeta['width'] > $imgmeta['height']) {
										$ratiox = $imgmeta['width'] / $imgmeta['height'];
										if($ratiox > 1.33) {
											$ratioLand = "ratioLand";
										}
									} else if($imgmeta['height'] > $imgmeta['width']) {
										$ratioy = $imgmeta['height'] / $imgmeta['width'];
										if($ratioy > 1.33) {
											$ratioPort = "ratioPort";
										}
									}
									if($ratioLand != "ratioLand" && $ratioPort != "ratioPort") {
										$ratioSquare = "ratioSquare";
									}
								 ?>

									<div class="promo-block flip-container <?php echo "$ratioLand $ratioPort $ratioSquare"; ?>" ontouchstart="this.classList.toggle('hover');">
										<div class="flipper">
											<div class="front">
												<div class="pimag"><img src="<?php echo $feat_image ?>"></div>
											</div>
											<?php if (get_the_content() != '') { ?>
											<div class="back">
												<div class="pimag"><img src="<?php echo $feat_image ?>"></div>
												<div class="promo-content">
													<div class="promo-inside" style="vertical-align: middle; display: table-cell; padding: 6px;"><?php echo  wpautop( get_the_content(), true ); ?></div>
												</div>
											</div>
											<?php } else { ?>
											<?php } ?>
										</div>
									</div>
								<?php $ratioLand = false; ?>
								<?php $ratioPort = false; ?>
								<?php $ratioSquare = false; ?>
								<?php endwhile; wp_reset_query(); ?>
					</div>
				</div>
				<div class="visible-xs hide" >
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<?php $loop = new WP_Query( array( 'post_type' => 'promociones', 'posts_per_page' => 20, 'order' => 'ASC' ) ); ?>
								<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

									<div class="promo-block" ontouchstart="this.classList.toggle('hover');">
										<div class="promo-inner">
											<div class="promo-front">

												<?php
													$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
												?>

												<?php echo "<div class=\"pimage\"><img src=\"$feat_image\"></div>" ?>
											</div>
											<?php if (get_the_content() != '') { ?>
											<div class="promo-top">
												<div class="promo-content">
													<div class="promo-inside" style="vertical-align: middle; display: table-cell; padding: 3px;"><p><?php echo  wpautop( get_the_content(), true ); ?></p></div>
												</div>
											</div>
											<?php } else { ?>
											<?php } ?>
										</div>
									</div>
								<?php endwhile; wp_reset_query(); ?>
							</div>
						</div>
					</div>
				</div>

<?php // ?>

				<div class="row" id="locate" style="padding-top: 35px !important;">
					<div class="col-sm-12">
						<div class="parContent">
							<h3 class="subt">ENCUENTRA TU TIENDA</h3>
						</div>

						<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/storelocator.css" />

						<div class="bh-sl-container">
							<div class="bh-sl-form-container">
								<form id="bh-sl-user-location" method="post" action="#">


									<div class="bh-sl-filters-container">
										<ul id="city-filter" class="bh-sl-filters">
											<li><h3>Ciudades</h3></li>
											<li>
												<select name="city" id="city" style="width: 150px;">
													<option value="">Todas</option>
													<option value="León">León</option>
													<option value="Querétaro">Querétaro</option>
													<option value="Distrito Federal">Distrito Federal</option>
													<option value="Culiacán">Culiacán</option>
													<option value="Guadalajara">Guadalajara</option>
													<option value="Mazatlán">Mazatlán</option>
													<option value="Monterrey">Monterrey</option>
													<option value="Puebla">Puebla</option>
													<option value="Boca del Río">Boca del Río</option>
													<option value="Playa del Carmen">Playa del Carmen</option>
													<option value="Estado de México">Estado de México</option>
												</select>
											</li>
										</ul>
										<ul id="plaza-filters-container1" class="bh-sl-filters">
											<li><h3>Plazas</h3></li>
											<li>
												<select name="plaza" id="plaza" style="width: 150px;">
													<option value="">Todas</option>
													<option value="ACOXPA">ACOXPA</option>
													<option value="ALTACIA">ALTACIA</option>
													<option value="ANTEA QUERÉTARO">ANTEA QUERÉTARO</option>
													<option value="CENTRO COMERCIAL PERISUR">CENTRO COMERCIAL PERISUR</option>
													<option value="COSMOPOL COACALCO">COSMOPOL COACALCO</option>
													<option value="FÓRUM BUENAVISTA">FÓRUM BUENAVISTA</option>
													<option value="FÓRUM CULIACÁN">FÓRUM CULIACÁN</option>
													<option value="FÓRUM TLAQUEPAQUE">FÓRUM TLAQUEPAQUE</option>
													<option value="GALERÍAS GUADALAJARA">GALERÍAS GUADALAJARA</option>
													<option value="GALERÍAS MAZATLÁN">GALERÍAS MAZATLÁN</option>
													<option value="GALERÍAS MONTERREY">GALERÍAS MONTERREY</option>
													<option value="GALERÍAS SERDÁN">GALERÍAS SERDÁN</option>
													<option value="GALERÍAS TOLUCA">GALERÍAS TOLUCA</option>
													<option value="GRAN PLAZA">GRAN PLAZA</option>
													
													<option value="OASIS COYOACÁN">OASIS COYOACÁN</option>
													<option value="OUTLET LERMA">OUTLET LERMA</option>

													
													<option value="PARQUE TEZONTLE">PARQUE TEZONTLE</option>
													<option value="PLAZA ANDAMAR">PLAZA ANDAMAR</option>
													<option value="PUNTA NORTE">PUNTA NORTE</option>
													<option value="QUINTA ALEGRÍA">QUINTA ALEGRÍA</option>
													<option value="SANTA FE">SANTA FE</option>
													
													<option value="TLALNE FASHION MALL">TLALNE FASHION MALL</option>
													<option value="TOREO PARQUE CENTRAL">TOREO PARQUE CENTRAL</option>
												</select>
											</li>
										</ul>
										<button id="bh-sl-submit" type="submit">BUSCAR</button>

									</div>
								</form>

							</div>

							<div id="map-container" class="bh-sl-map-container">
								<div class="bh-sl-loc-list">
									<ul class="list"></ul>
								</div>
								<div id="bh-sl-map" class="bh-sl-map"></div>
							</div>
						</div>


						<script src="<?php bloginfo('template_directory') ?>/assets/js/libs/handlebars.min.js"></script>
						<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
						<script src="<?php bloginfo('template_directory') ?>/assets/js/plugins/storeLocator/jquery.storelocator.js"></script>


						<script>

							var zIndex = 999;

							$(function(){

								$('.promo-block').mouseenter( function(e){

									$(this).css('z-index', zIndex);
									zIndex++;

								} );

							});

						</script>

						<script>
							var tempCity;
							var redFlag = false;
							var xml;
							var xmlObj;






							$(function() {


								$('#bh-sl-submit').click(function(e){
									e.preventDefault();

									//xmlParser($(xmlObj), tempCity);

								})
								$('#map-container').storeLocator({
									'mapSettings': {
										maxZoom: 15,
										minZoom: 5
									},
									'taxonomyFilters' : {
										'city' : 'city-filter',
										'plaza' : 'plaza-filters-container1'
									},
									'callbackNoResults': function() {
									},
									'dataLocation': '<?php bloginfo('template_directory') ?>/data/locations-new.xml',
									'infowindowTemplatePath': '<?php bloginfo('template_directory') ?>/assets/js/plugins/storeLocator/templates/infowindow-description.html',
									'listTemplatePath': '<?php bloginfo('template_directory') ?>/assets/js/plugins/storeLocator/templates/location-list-description.html',
									'KMLinfowindowTemplatePath': '<?php bloginfo('template_directory') ?>/assets/js/plugins/storeLocator/templates/kml-infowindow-description.html',
									'KMLlistTemplatePath': '<?php bloginfo('template_directory') ?>/assets/js/plugins/storeLocator/templates/kml-location-list-description.html'
								});

								$.ajax({
								    type: "GET",
								    url: "<?php bloginfo('template_directory') ?>/data/locations-new.xml",
								    dataType: "xml",
								    success: function(xml) {
								    	xmlObj = xml;
								    	$(xml).find("markers marker").each(function () {
								    		$("#plaza").append('<option value="' + $(this).attr('plaza') +   '">' + $(this).attr('plaza') +   '</option>');
								    	});
								    	tempCity = $('#city').children('option:selected')[0].value;
								    	xmlParser($(xml), tempCity);
								    }
								});

								function xmlParser(xml, tempCity) {
									$('#plaza').html('');
									$('#plaza').append('<option value="" select >Todas</option>');
									$('#plaza').children('option:eq(0)').prop('selected', true).trigger('change');
									$(xml).find("markers marker").each(function () {

										if(tempCity != '') {
											if($(this).attr('city') == tempCity) {
												console.log("bien");
												$("#plaza").append('<option value="' + $(this).attr('plaza') +   '">' + $(this).attr('plaza') +   '</option>');
											}
										}
										else {
											console.log("talve");
											redFlag = true;
										}

									 });
									if(redFlag == true) {
										$(xml).find("markers marker").each(function () {
											$("#plaza").append('<option value="' + $(this).attr('plaza') +   '">' + $(this).attr('plaza') +   '</option>');
										});
										redFlag = false;
									}


								}

								$('#city').on('change', function(xml){
									$('#plaza').children('option:eq(0)').prop('selected', true).trigger('change');
									tempCity = $(this).children('option:selected')[0].value;
									xmlParser($(xmlObj), tempCity);
								});
							});
						</script>

						<?php while ( have_posts() ) : the_post(); ?>
							<?php /* echo do_shortcode( '[wpsl]' ) */ ?>
							<?php /* echo do_shortcode( '[SLPLUS]' ) */ ?>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
			<?php //bloginfo('template_directory') ?>

<?php get_footer(); ?>
