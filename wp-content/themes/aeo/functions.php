<?php
/**
 * AEO functions and definitions
 *
 * @package AEO
 */

if ( ! function_exists( 'aeo_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function aeo_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on AEO, use a find and replace
	 * to change 'aeo' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'aeo', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'aeo' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'aeo_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // aeo_setup
add_action( 'after_setup_theme', 'aeo_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function aeo_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'aeo_content_width', 640 );
}
add_action( 'after_setup_theme', 'aeo_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function aeo_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'aeo' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'aeo_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function aeo_scripts() {
	wp_enqueue_style( 'aeo-style', get_stylesheet_uri() );

	wp_enqueue_script( 'aeo-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'aeo-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'aeo_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


add_filter( 'body_class', 'my_neat_body_class');
function my_neat_body_class( $classes ) {
	if ( is_page(11) )
	     $classes[] = 'tech';
	 if ( is_page(13) )
	      $classes[] = 'mwy';
     return $classes;
}






// Register Custom Post Type
function promociones() {

	$labels = array(
		'name'                => _x( 'Promociones', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Promoción', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Promoción', 'text_domain' ),
		'name_admin_bar'      => __( 'Promoción', 'text_domain' ),
		'parent_item_colon'   => __( 'Promoción Padre:', 'text_domain' ),
		'all_items'           => __( 'Todos los items', 'text_domain' ),
		'add_new_item'        => __( 'Agregar nuevo item', 'text_domain' ),
		'add_new'             => __( 'Agregar nuevo', 'text_domain' ),
		'new_item'            => __( 'Nuevo item', 'text_domain' ),
		'edit_item'           => __( 'Editar item', 'text_domain' ),
		'update_item'         => __( 'Actualizar item', 'text_domain' ),
		'view_item'           => __( 'Ver item', 'text_domain' ),
		'search_items'        => __( 'Buscar item', 'text_domain' ),
		'not_found'           => __( 'No se encontró', 'text_domain' ),
		'not_found_in_trash'  => __( 'No se encontró en la papelera', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'promociones', 'text_domain' ),
		'description'         => __( 'Banners de Promociones #moveswithyou', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'promociones', $args );

}

// Hook into the 'init' action
add_action( 'init', 'promociones', 0 );


// Gravity Forms anchor - disable auto scrolling of forms
add_filter("gform_confirmation_anchor", create_function("","return false;"));

