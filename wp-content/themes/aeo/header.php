<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package AEO
 */

?>

</head>

	<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<title>AEO TECHNOLOGY</title>
			<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bigvideo.css">
			<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
			<link href="<?php echo get_template_directory_uri(); ?>/css/jquery-ui.min.css" rel="stylesheet">
			<link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">
			<link href="<?php echo get_template_directory_uri(); ?>/css/style.css?v=1.2" rel="stylesheet">
			<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
				<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
			<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.min.js"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/js/imagesloaded.pkgd.min.js"></script>
			<script src="http://vjs.zencdn.net/4.3/video.js"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/js/bigvideo.js"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
			<!--[if IE 9]> <script type="text/javascript"> jQuery(document).ready(function($) { _V_.options.techOrder = ["flash", "html5", "links"]; }); </script> <![endif]-->
			<script src="<?php echo get_template_directory_uri(); ?>/assets/js/polys/cssfx.js"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/assets/js/polys/respond.js"></script>
		<?php wp_head(); ?>
		
		<script>
		 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		 ga('create', 'UA-43305108-5', 'auto');
		 ga('send', 'pageview');
		</script>

		</head>
		<body <?php body_class(); ?> id="<?php if(is_page('moveswithyou')) echo 'moveswithyou'; ?>">

			<div class="container-fluid">
				<div class="header clearfix">
					<nav class="navbar navbar-default navbar-fixed-top">
					  <div class="container">
					    <!-- Brand and toggle get grouped for better mobile display -->
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					      <a onmousedown="ga('send', 'event', 'Menu', 'click', 'LogoAEO');" class="navbar-brand" href="<?php bloginfo('url'); ?>">AMERICAN EAGLE OUTFITTERS</a>
					    </div>

					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="navbar">
					      <ul class="nav navbar-nav navbar-right">
					      	

					      		<?php if(is_front_page()): ?>


						      	<li class="active"><a onmousedown="ga('send', 'event', 'Menu', 'click', 'JeansAEO');" href="<?php bloginfo('url'); ?>">JEANS AEO</a></li>
						      	<?php else:	?>
						      	<li><a onmousedown="ga('send', 'event', 'Menu', 'click', 'JeansAEO');" href="<?php bloginfo('url'); ?>">JEANS AEO</a></li>
						      <?php endif; ?>
						      <?php if(is_page('moveswithyou')) { ?>
							      <!--<li class="active"><a onmousedown="ga('send', 'event', 'Menu', 'click', 'MovesWithyou');" id="mwy" href="#moveswithyou">AEO CARD</a></li>-->
							      <li><a onmousedown="ga('send', 'event', 'Menu', 'click', 'EncuentraTuTienda');" href="#locate">ENCUENTRA TU TIENDA</a></li>
							  <?php } else { ?>
								  <!--<li><a onmousedown="ga('send', 'event', 'Menu', 'click', 'MovesWithyou');" id="mwy" href="moveswithyou">AEO CARD</a></li>-->
								  <li><a onmousedown="ga('send', 'event', 'Menu', 'click', 'EncuentraTuTienda');" href="moveswithyou#locate">ENCUENTRA TU TIENDA</a></li>
							  <?php } ?>

							  <li class="social">
					      	<ul>
					      		<li onmousedown="ga('send', 'event', 'RedesSociales', 'click', 'Facebook');" class="fb"><a href="https://www.facebook.com/americaneaglemexico" target="_blank"><i class="fa fa-facebook"></i></a></li>
					      		<li onmousedown="ga('send', 'event', 'RedesSociales', 'click', 'Twitter');" class="tw"><a href="https://twitter.com/aeo_mx" target="_blank"><i class="fa fa-twitter"></i></a></li>
					      	</ul>
					      </li>
					      </ul>
					    </div><!-- /.navbar-collapse -->
					  </div><!-- /.container-fluid -->
					</nav>
				</div>