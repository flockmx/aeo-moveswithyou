<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package AEO
 */

?>



<div class="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul>
<!-- 				<li><a href="http://es.ae.com/web/help/terms_of_use.jsp?topic=1&navdetail=footer:utility:p1">CONDICIONES DE USO</a></li>
				<li><a href="http://es.ae.com/web/help/privacy_policy.jsp?topic=5&navdetail=footer:utility:p2">CUMPLIMIENTO CON LA NORMATIVA DE CALIFORNIA</a></li>
 -->				<li><a target="_blank" href="http://es.ae.com/web/help/privacy_policy.jsp?topic=7">POLÍTICAS DE PRIVACIDAD</a></li>
<!-- 				<li><a href="http://es.ae.com/web/sitemap/index.jsp?navdetail=footer:utility:p5">MAPA DE SITIO</a></li>
				<li><a href="http://es.ae.com/web/help/brand_protection.jsp?navdetail=footer:utility:p6">PROTECCIÓN DE LAS MARCAS</a></li>
				<li><a href="https://signup.cj.com/member/brandedPublisherSignUp.do?air_refmerchantid=4445720">PROGRAMA DE AFILIADOS</a></li> -->
				</ul>
<!-- 				<ul>
				<li><a href="http://es.ae.com/web/lp/tees.jsp?navdetail=footer:utility:p9">T-SHIRTS</a></li>
				<li><a href="http://es.ae.com/web/factory/store.jsp?navdetail=footer:utility:p10">AEO FACTORY STORE</a></li>
				</ul>
 -->				<small>© 2015 AEO MANAGEMENT CO. TODOS LOS DERECHOS RESERVADOS.</small>
				<img src="<?php echo get_template_directory_uri(); ?>/imgs/logos.png">
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
var templateUrl = '<?= get_bloginfo("template_url"); ?>';

$('#wpsl-search-btn').on('click', function() {
	ga('send', 'event', 'EncuentraTuTienda', 'click', 'Buscar');
});
$('.slp_ui_button').on('click', function() {
	ga('send', 'event', 'EncuentraTuTienda', 'click', 'Buscar');
});
$('.gform_button').on('click', function() {
	ga('send', 'event', 'Promo50%', 'click', 'Enviar');
});

$('#bh-sl-submit').on('click', function() {
	ga('send', 'event', 'EncuentraTuTienda', 'click', 'Buscar');
});

</script>

		<?php wp_footer(); ?>

		<script src="<?php bloginfo('template_directory') ?>/assets/js/viewport-units-buggyfill-master/viewport-units-buggyfill.js"></script>
		<script>window.viewportUnitsBuggyfill.init();</script>

	</body>
</html>

