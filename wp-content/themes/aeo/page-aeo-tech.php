<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package AEO
 */

get_header(); ?>

			<div class="vidMain">
				<div class="vidContainer ios">
				</div>
				<div class="vidContent">
					<h1>PRESENTANDO</h1>
					<h1 class="hidden-xs"><img src="<?php echo get_template_directory_uri(); ?>/imgs/tech.png"></h1>
					<h1 class="visible-xs"><img src="<?php echo get_template_directory_uri(); ?>/imgs/tech-v.png"></h1>
<!-- 					<p><a onmousedown="ga('send', 'event', 'Video', 'click', 'HomeReproducir');" class="btn btn-lg btn-default" id="video-toggle" href="#" role="button">REPRODUCIR VIDEO</a></p> -->
				</div>
			</div>

<div class="party hide" onclick="ga('send', 'event', 'SummerTripVenice', 'click', 'Aqui');" data-toggle="modal" data-target="#partyModal" style="text-transform:uppercase;">
	No te pierdas la última fiesta de nuestro Summer Trip al estilo Venice Beach. Entérate cómo puedes asistir.<br> <strong>DA CLIC AQUÍ</strong>
</div>


<div class="modal fade" id="partyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
	      
	     
	      
        <h5>Compra unos Flex/Denim o Denim X</h5>
       <!-- <p>en Acoxpa, Buenavista, Perisur, Santa Fe o Toreo,</p> <p>Muestra una identificación oficial y ya tienes un boleto doble para asistir a nuestra segunda fiesta el 8 de Octubre en el Foro Indie Rocks a partir de las 8 pm.</p> -->
        	<p>en Acoxpa, Buenavista, Oasis Coyoacán, Parque Tezontle, Perisur, Santa Fe o Toreo.</p>  <p>-Acceso al evento hasta las 23:30hrs-</p>

        <h5>#MovesWithYou</h5> 
      </div>
    </div>
  </div>
</div>

<div class="container-fluid hidden-xs">
	<div class="row">
		<div class="col-sm-6 color-m">
			<img src="<?php echo get_template_directory_uri(); ?>/imgs/df-side.jpg" alt="">
			<img src="<?php echo get_template_directory_uri(); ?>/imgs/df-top.png" alt="">

			    				<div class="block-item">
			    					<div class="brand-block">
				    					<h5>NIVEL 1:</h5>
										<h4>CORE FLEX</h4>
										<!--<p>Se ven y se sienten como tus jeans AEO favoritos, <br/>ahora con la nueva tecnología flex. <br> La menor cantidad de stretch pero con mayor flexibilidad.</p>-->
										<p>SE VEN Y SE SIENTE COMO TUS JEANS AEO FAVORITOS, NUEVA TECNOLOGÍA FLEX, MENOR CANTIDAD DE STRETCH, MAYOR FLEXIBILIDAD. </p>
			    					</div>
			    				</div>

			    				<div class="block-item">
			    					<div class="brand-block">
				    					<h5>NIVEL 2:</h5>
										<h4>ACTIVE FLEX</h4>
										<p>Mayor flexibilidad y movimiento. <br> Se ajusta y mantiene su forma.</p>
			    					</div>
			    				</div>

			    				<div class="block-item">
			    					<div class="brand-block">
				    					<h5>NIVEL 3:</h5>
										<h4>EXTREME FLEX</h4>
										<p>Combina hilos Dual Fx® y T400 para mayor nivel de flexibilidad. Mezclilla súper stretch que mantiene su forma todo el día.</p>
			    					</div>
			    				</div>
		</div>
		<div class="col-sm-6 color-w">
			<img src="<?php echo get_template_directory_uri(); ?>/imgs/dx-side.jpg" alt="">
			<img src="<?php echo get_template_directory_uri(); ?>/imgs/dx-top.png" alt="">

			<div class="block-item">
				<div class="brand-block">
					<h4>DENIM X</h4>
					<p>Utiliza tecnología Dual Fx® de lycra® con fibras naturales que lo hacen suave al tacto.</p>
				</div>
			</div>

			<div class="block-item">
				<div class="brand-block">
					<h4>DENIM X<sup>4</sup></h4>
					<p>Ajuste revolucionario de 4 niveles, flexibilidad <br/>mejorada y movimiento 360°</p>
				</div>
			</div>

			<div class="block-item">
				<div class="brand-block">
					<h4>SATEEN X</h4>
					<p>Tejido con hilos de baja torción y acabado Sateen <br/>con súper ajuste que los hace aún más cómodos.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 esp">
			<div class="mail-block flip-container" ontouchstart="this.classList.toggle('hover');" style="display:none;">
				<div class="flipper">
					<div class="front">
						<p>¡El verano sigue y los descuentos también!</p>
					</div>
					<div class="back">
							<?php gravity_form(1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>


<div class="container-fluid visible-xs">
	<div>

	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" id="myTabs" role="tablist">
	    <li role="presentation" class="color-m"><a href="#denimflex" aria-controls="denimflex" role="tab" data-toggle="tab">DENIM FLEX</a></li>
	    <li role="presentation" class="color-w"><a href="#denimx" aria-controls="denimx" role="tab" data-toggle="tab">DENIM X</a></li>
	  </ul>

	  <!-- Tab panes -->
	  <div class="tab-content">
	    <div role="tabpanel" class="tab-pane" id="denimflex">
	    	<div class="row">
	    		<div class="col-sm-6 color-m">
	    		<img src="<?php echo get_template_directory_uri(); ?>/imgs/df-side.jpg" alt="">
	    		<img src="<?php echo get_template_directory_uri(); ?>/imgs/df-top.png" alt="">
	    		</div>
	    	</div>




	    	<div class="container-fluid">
	    		<div class="row">
	    			<div class="col-sm-6 color-m">
	    				
	    				<img src="<?php echo get_template_directory_uri(); ?>/imgs/aeo-02.png" alt="">
	    				<div class="block-item">
	    					<div class="brand-block">
	    						
	    						<h5>NIVEL 1:</h5>
								<h4>CORE FLEX</h4>
								<p>SE VEN Y SE SIENTE COMO TUS JEANS AEO FAVORITOS, NUEVA TECNOLOGÍA FLEX, MENOR CANTIDAD DE STRETCH, MAYOR FLEXIBILIDAD. </p>
								
	    					</div>

	    				</div>
						<img src="<?php echo get_template_directory_uri(); ?>/imgs/aeo-03.png" alt="">
	    				<div class="block-item">
	    					
	    					<div class="brand-block">
	    						
		    					<h5>NIVEL 2:</h5>
								<h4>ACTIVE FLEX</h4>
								<p>Mayor flexibilidad y movimiento. <br> Se ajusta y mantiene su forma.</p>
	    					</div>
	    				</div>
						
						<img src="<?php echo get_template_directory_uri(); ?>/imgs/aeo-04.png" alt="">
	    				<div class="block-item">
	    					
	    					<div class="brand-block">
		    					<h5>NIVEL 3:</h5>
								<h4>EXTREME FLEX</h4>
								<p>Combina hilos Dual Fx® y T400 para mayor nivel de flexibilidad. Mezclilla súper stretch que mantiene su forma todo el día.</p>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>


	    	<div class="mail-block flip-container" ontouchstart="this.classList.toggle('hover');" style="display:none;">
	    		<div class="flipper">
	    			<div class="front">
	    				<p>¡El verano sigue y los descuentos también!</p>
	    			</div>
	    			<div class="back">
	    					<?php gravity_form(1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex ); ?>
	    			</div>
	    		</div>
	    	</div>

	    </div>
	    <div role="tabpanel" class="tab-pane" id="denimx">
	    	<div class="row">
	    		<div class="col-sm-6 color-w">
		    		<img src="<?php echo get_template_directory_uri(); ?>/imgs/dx-side.jpg" alt="">
		    		<img src="<?php echo get_template_directory_uri(); ?>/imgs/dx-top.png" alt="">
	    		</div>
	    	</div>




	    	<div class="container-fluid">
	    		<div class="row">
	    			<div class="col-sm-6 color-w">
	    				<div class="block-item">
	    					<div class="brand-block">
								<h4>DENIM X</h4>
								<p>Utiliza tecnología Dual Fx® de lycra® con fibras naturales que lo hacen suave al tacto.</p>
	    					</div>
	    				</div>
						
						<img src="<?php echo get_template_directory_uri(); ?>/imgs/aeo-x-01.png" alt="">
	    				<div class="block-item">
							<div class="brand-block">
								<h4>DENIM X<sup>4</sup></h4>
								<p>Ajuste revolucionario de 4 niveles, flexibilidad mejorada y movimiento 360°</p>
							</div>
	    				</div>

						<img src="<?php echo get_template_directory_uri(); ?>/imgs/aeo-x-02.png" alt="">
	    				<div class="block-item">
	    					<div class="brand-block">
								<h4>SATEEN X</h4>
								<p>Tejido con hilos de baja torción y acabado Sateen con súper ajuste que los hace aún más cómodos.</p>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>

	    	<div class="mail-block flip-container" ontouchstart="this.classList.toggle('hover');" style="display:none;">
	    		<div class="flipper">
	    			<div class="front">
						<p>¡El verano sigue y los descuentos también!</p>
	    			</div>
	    			<div class="back">
	    					<?php gravity_form(1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex ); ?>
	    			</div>
	    		</div>
	    	</div>

	    </div>
	  </div>

	</div>



	<div class="mail-block flip-container" ontouchstart="this.classList.toggle('hover');" style="display:none;">
		<div class="flipper">
			<div class="front">
						<p>¡El verano sigue y los descuentos también!</p>
			</div>
			<div class="back">
				<?php gravity_form(1, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = true, $tabindex ); ?>
			</div>
		</div>
	</div>

</div>


<?php get_footer(); ?>
