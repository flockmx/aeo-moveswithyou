=== Store Locator Plus : Pro Pack ===
Plugin Name:  Store Locator Plus : Pro Pack
Contributors: charlestonsw
Donate link: http://www.storelocatorplus.com/product/slp4-pro/
Tags: store locator plus, pro pack
Requires at least: 3.8
Tested up to: 4.2.2
Stable tag: 4.2.15

A premium add-on for the Store Locator Plus location management system for WordPress.

== Description ==

This plugin will help site admins manage large listings of locations with tools for processing location data en mass.

== Installation ==

= Requirements =

* Store Locator Plus: 4.2.58+
* WordPress: 4.0+
* PHP: 5.2.4+

= Install After SLP =

1. Go fetch and install the latest version of Store Locator Plus.
2. Purchase this plugin from the [Store Locator Plus website](http://www.storelocatorplus.com) to get the latest .zip file.
3. Go to plugins/add new.
4. Select upload.
5. Upload the zip file.

== Frequently Asked Questions ==

= What are the terms of the license? =

The license is GPL.  You get the code, feel free to modify it as you wish.
Hopefully you like it enough to want to support the effort to bring useful software to market.
Learn more on the [CSA License Terms](http://www.storelocatorplus.com/products/general-eula/) page.

== Changelog ==

Visit the [Online Change Log for details](http://www.storelocatorplus.com/tag/slp4-changelog/).

= 4.2.15 (June-2015) =

* Fix: Reporting on search results are no longer truncated when location data contains special characters.

= 4.2.14 (June-2015) =

* Fix: Fix the field shifting on CSV exports.
* Enhancement: Update the scheduled file import to use WordPress remote get.  Gets around systems with heightened security that block php.ini allow_fopen_url as long as they have cUrl enabled.
* Enhancement: Increase the schedule file import timeout to 300 seconds to fetch a larger remote CSV file.
* Enhancement: Remove defunct method call for manage locations admin interface.
* Enhancement: Update state selector filter to use lower-memory-consumption data select methods on admin panel.

= 4.2.13 (May-18-2015) =

* Enhancement: Improve export memory allocation by working around built-in WordPress full data set caching.  Prevents memory exhaustion on most servers.
* Enhancement: Improve the locally hosted CSV export, provide a link directly to the file for download after export is complete.
* Enhancement: Remove unused properties from classes.


= 4.2.11 (May-1-2015) =

* Change: Update Admin and make compatible with SLP 4.2.48.

= 4.2.10 (Apr-21-2015) =

* Enhancement: Updated nl_NL language files.

= 4.2.09 (Apr-20-2015) =

* Fix: Patch to work with SLP 4.2.44 COREURL reference removal.

= 4.2.08 (Apr-2015) =

* Change: Requires SLP 4.2.43
* Enhancement: Better CSV import file header testing prior to import.
* Enhancement: Reinstate detailed import notices.
* Enhancement: Improve CSV import warnings and errors.

= 4.2.07 (Mar-2015) =

* Change:Requires SLP 4.2.41
* Enhancement: Add a recode all uncoded option to manage locations bulk action.
* Enhancement: Create a LOAD DATA import for CSV files, works with basic location data only.

= 4.2.04 (Feb-2015) =

* Enhancement: Trim extra whitespace from CSV import data.
* Enhancement: Use php maximum execution time for CSV processing.
* Enhancement: Implement standard AJAX class subsystem for extra AJAX request security.

= 4.2.03 (Nov-2014) =

* Enhancement: Added scheduled import of remote location CSV files.

= 4.2.02 (Nov-2014) =

* Enhancement: Add Swedish (sv_SE) translation by [Piani Sweden AB](http://piani.se).

= 4.2.01 (Oct-2014) =

* Fix: Make the report queries PHP 5.5+ compatible by passing them through wpdb prepare statements.
* Enhancement: Add a remote CSV file fetching option to grab CSV files from remote http/https servers.
* Enhancement: Set PHP execution time to 10 minutes during location exports if the server allows for it.

= 4.2.00 (Aug-2014) =

* Note: Requires SLP 4.2.04
* Change: Pro Pack / Tools has been removed.
* Change: Pro Pack / Look and Feel / Custom CSS has been moved to User Experience / View.
* Change: Pro Pack / Reporting / Enable Reporting has been moved to Reporting.
* Change: Location CSV Import defaults to update on duplicates and first line has field name enabled.
* Enhancement: Update to use SLP 4.2 add-on pack system, decreasing the memory footprint and PHP processing load.
* Enhancement: tags_for_dropdown is now an alias for shortcode attribute tags_for_pulldown.
* Enhancement: Convert reporting interface to standard SLP object type for improved future report improvements.
* Fix; Highlight Uncoded on Manage Locations on/off setting under General Settings (requires SLP 4.2.06+).
