<?php

/**
 * Plugin Name: Store Locator Plus : Pro Pack
 * Plugin URI: http://www.storelocatorplus.com/product/slp4-pro/
 * Description: A premium add-on pack for Store Locator Plus that provides more admin power tools for wrangling locations.
 * Version: 4.2.15
 * Author: Store Locator Plus
 * Author URI: http://www.storelocatorplus.com/
 * Requires at least: 3.8
 * Tested up to : 4.2.2
 *
 * Text Domain: csa-slp-pro
 * Domain Path: /languages/
 */

// Exit if access directly, dang hackers
if (!defined('ABSPATH')) {
    exit;
} 
// No SLP? Get out...
//
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if (!function_exists('is_plugin_active') || !is_plugin_active('store-locator-le/store-locator-le.php')) {
    return;
}

// Make sure the class is only defined once.
//
if (!class_exists('SLPPro')) {
    require_once( WP_PLUGIN_DIR . '/store-locator-le/include/base_class.addon.php');

    /**
     * The Pro Pack Add-On Pack for Store Locator Plus.
     *
     * @package StoreLocatorPlus\ProPack
     * @author Lance Cleveland <lance@charlestonsw.com>
     * @copyright 2014-2015 Charleston Software Associates, LLC
     */
    class SLPPro extends SLP_BaseClass_Addon {

        //-------------------------------------
        // Properties
        //-------------------------------------

        /**
         * @var \SLPPro_Admin
         */
        public $admin;

        /**
         * If a cron job is running this points to the object handling it.
         *
         * @var \SLPPro_Cron
         */
        public $cron_job;

        /**
         * The csvExporter object.
         *
         * @var \CSVExportLocations $csvExporter
         */
        public $csvExporter;

        /**
         * The csvImporter object.
         *
         * @var \CSVImportLocations $csvImporter
         */
        public $csvImporter;

        /**
         * A copy of the SLPPro object once instantiated.
         *
         * @var \SLPPro
         */
        public static $instance;

        /**
         * Settable options for this plugin. (Does NOT go into main plugin JavaScript)
         *
         * Admin Panel Settings
         * o highlight_uncoded if on the non-geocoded locations are highlighted on the manage locations table.
         *
         * UI View
         * o layout the overall locator page layout, uses special shortcodes, default set by base plugin \SLPlus class.
         *
         * UI Search
         * o tag_label text that appears before the form label
         * o tag_selector style of search form input 'none','hidden','dropdown','textinput'
         * o tag_selections list of drop down selections
         *
         * UI Results
         * o tag_output_processing how tag data should be pre-processed when sending JSONP response to front end UI.
         *
         * CSV Processing
         * o csv_duplicates_handling = how to handle incoming records that match name + add/add2/city/state/zip/country
         * oo add = add duplicate records (default)
         * oo skip = skip over duplicate records
         * oo update = update duplicate records, requires id field in csv file to update name or address fields
         *
         * Program Control Settings
         * o installed_version - set when the plugin is activated or updated
         *
         * @var mixed[] $options
         */
        public $options = array(
            'cron_import_timestamp'         => '',
            'cron_import_recurrence'        => 'none',
            'csv_file_url'                  => '',
            'csv_first_line_has_field_name' => '1',
            'csv_skip_first_line'           => '0',
            'csv_skip_geocoding'            => '0',
            'csv_duplicates_handling'       => 'update',
            'load_data'                     => '0',
            'custom_css'                    => '',
            'highlight_uncoded'             => '1',
            'installed_version'             => '',
            'layout'                        => '',
            'tag_label'                     => '',
            'tag_selector'                  => 'none',
            'tag_selections'                => '',
            'tag_output_processing'         => 'as_entered',
            'reporting_enabled'             => '0'
        );

        //------------------------------------------------------
        // METHODS
        //------------------------------------------------------

        /**
         * Invoke the plugin.
         *
         * This ensures a singleton of this plugin.
         *
         * @static
         */
        public static function init() {
            static $instance = false;
            if (!$instance) {
                load_plugin_textdomain('csa-slp-pro', false, dirname(plugin_basename(__FILE__)) . '/languages/');
                $instance = new SLPPro(
                    array(
                        'version'               => '4.2.15'                                 ,
                        'min_slp_version'       => '4.2.58'                                 ,

                        'name'                  => __('Pro Pack', 'csa-slp-pro')            ,
                        'slug'                  => plugin_basename(__FILE__)                ,
                        'option_name'           => 'csl-slplus-PRO-options'                 ,
                        'metadata'              => get_plugin_data(__FILE__, false, false)  ,

                        'url'                   => plugins_url('', __FILE__)                ,
                        'dir'                   => plugin_dir_path(__FILE__)                ,

                        'activation_class_name'     => 'SLPPro_Activation'                  ,
                        'admin_class_name'          => 'SLPPro_Admin'                       ,
                        'ajax_class_name'           => 'SLPPro_AJAX'                        ,
                        'userinterface_class_name'  => 'SLPPro_UI'                          ,
                    )
                );
            }

            return $instance;
        }

        /**
         * Convert an array to CSV.
         *
         * @param array[] $data
         * @return string
         */
        static function array_to_CSV($data) {
            $outstream = fopen("php://temp", 'r+');
            fputcsv($outstream, $data, ',', '"');
            rewind($outstream);
            $csv = fgets($outstream);
            fclose($outstream);
            return $csv;
        }


        /**
         * Create and attach the \CSVExportLocations object
         */
        function create_CSVLocationExporter() {
            if (!class_exists('CSVExportLocations')) {
                require_once(plugin_dir_path(__FILE__) . 'include/class.csvexport.locations.php');
            }
            if (!isset($this->csvExporter)) {
                $this->csvExporter =
                    new CSVExportLocations(
                        array(
                            'addon'     => $this   ,
                            'slplus'    => $this->slplus,
                            'type'      => 'Locations'
                        )
                    );
            }
        }

        /**
         * Create and attach the \CSVImportLocations object
         */
        function create_CSVLocationImporter() {
            if (!class_exists('CSVImportLocations')) {
                require_once(plugin_dir_path(__FILE__) . 'include/class.csvimport.locations.php');
            }
            if (!isset($this->csvImporter)) {
                $this->csvImporter =
                    new CSVImportLocations(
                        array(
                            'addon'    => $this   ,
                            'slplus'    => $this->slplus
                        )
                    );
            }
        }

        /**
         * Create a Map Settings Debug My Plugin panel.
         *
         * @return null
         */
        static function create_DMPPanels() {
            if (!isset($GLOBALS['DebugMyPlugin'])) {
                return;
            }
            if (class_exists('DMPPanelSLPPro') == false) {
                require_once(plugin_dir_path(__FILE__) . 'include/class.dmppanels.php');
            }
            $GLOBALS['DebugMyPlugin']->panels['slp.pro'] = new DMPPanelSLPPro();
        }

        /**
         * Simplify the plugin debugMP interface.
         *
         * @param string $type
         * @param string $hdr
         * @param string $msg
         */
        function debugMP($type, $hdr, $msg = '') {
            if (!is_object($this->slplus)) {
                return;
            }
            $this->slplus->debugMP('slp.pro', $type, $hdr, $msg, NULL, NULL, true);
        }

        /**
         * Set the admin menu items.
         *
         * @param mixed[] $menuItems
         * @return mixed[]
         */
        public function filter_AddMenuItems( $menuItems ) {
            $this->createobject_Admin();
            $this->admin_menu_entries = array(
                array(
                    'label'         => __('Reports', 'csa-slp-pro'),
                    'slug'          => 'slp_reports',
                    'class'         => $this->admin->reports,
                    'function'      => 'render_ReportsTab'
                ),
            );
            return parent::filter_AddMenuItems( $menuItems );
        }

        /**
         * Process a cron job.
         *
         * WordPress is not a "time perfect" cron processor.   It will fire the event the next time a visitor
         * comes to the site AFTER the specified cron job time.
         *
         * Action Parameter
         * - 'import_csv' : import the csv locations file, params needs to be a file_meta named array
         *
         * @param $action
         * @param $params
         */
        static function process_cron_job( $action , $params ) {
            if ( class_exists( 'SLPPro_Cron' ) == false ) {
                require_once(plugin_dir_path(__FILE__) . 'include/class.cron.php');
            }

            $pro_instance = SLPPro::init();

            $pro_instance->cron_job = new SLPPro_Cron(
                    array(
                        'action'        => $action ,
                        'action_params' => $params ,
                        'addon'         => $pro_instance,
                    )
                );
        }

    }

    // Hook to invoke the plugin.
    //
    add_action('init', array('SLPPro', 'init'));

    // CRON Jobs
    //
    add_action('cron_csv_import' , array( 'SLPPro' , 'process_cron_job') , 10 , 2 );

    // DMP
    //
    add_action('dmp_addpanel', array('SLPPro', 'create_DMPPanels'));
}
// Dad. Explorer. Rum Lover. Code Geek. Not necessarily in that order.
